from django.http import HttpResponse, JsonResponse, HttpResponseRedirect
from django.shortcuts import render, redirect
from django.template.loader import render_to_string
from django.urls import reverse_lazy
from django.views.generic import ListView
from django.views.generic.edit import UpdateView, CreateView, DeleteView
from .forms import *
from .models import Pais

class PaisListCreateView(CreateView):
    model = Pais
    form_class = PaisMF
    template_name = 'teste/pais-listarcriar.html'
    success_url = reverse_lazy('teste:pais-listarcriar')

    def get_context_data(self, ** kwargs):
        context = super(PaisListCreateView, self).get_context_data(**kwargs)
        context['object_list'] = self.model.objects.all()
        return context

def paisDelete(request):
    print('entrou no deletar')
    print(request.POST.get('id'))
    if request.method == 'POST' and request.POST.get('id') != None :
        pais = request.POST.get('id')
        pais_deletar = Pais.objects.get(id=pais)
        pais_deletar.delete()
        print('deletado')
        return HttpResponseRedirect('/pais/listarcriar')
    return redirect('/pais/listarcriar')

def paisUpdate(request):
    print('entrou no editar')
    print(request.POST.get('id'))
    if request.method == 'POST' and request.POST.get('id') != None :
        pais = request.POST.get('id')
        pais_editar = Pais.objects.get(id=pais)
        pais_editar.pais = request.POST.get('pais')
        if request.FILES.get('bandeira') == None:
            pais_editar.bandeira = request.POST.get('bandeiraexiste')[7:]
        else:
            pais_editar.bandeira = request.FILES.get('bandeira')
        pais_editar.save()
        print('editado')
        return HttpResponseRedirect('/pais/listarcriar')
    return redirect('/pais/listarcriar')
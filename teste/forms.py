from django.forms import ModelForm
from .models import *

class PaisMF(ModelForm):
    class Meta:
        model = Pais
        fields = '__all__'

class EstadoMF(ModelForm):
    class Meta:
        model = Estado
        fields = '__all__'